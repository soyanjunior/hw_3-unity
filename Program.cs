﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("The game begins");

        Random random = new Random();
        Console.Write("Player 1, enter your name\n");
        string User1 = Console.ReadLine();

        Console.Write("Player 2, enter your name\n");
        string User2 = Console.ReadLine();

        string activeUser = string.Empty;
        int gameNumber = random.Next(12, 120);
        int userTry;
        int minUserTry = 1;
        int maxUserTry = 4;
        Console.WriteLine($"Hidden number: {gameNumber}");
        while (gameNumber > 0)
        {
            activeUser = activeUser != User1 ? User1 : User2;
            Console.WriteLine($"{activeUser} enter a number: ");
            userTry = Convert.ToInt32(Console.ReadLine());
                        
            if (userTry < minUserTry || userTry > maxUserTry || userTry > gameNumber)
            {
                Console.WriteLine("Uncorrect number! Enter a number again");
                userTry = int.Parse(Console.ReadLine());
            }
            else
            {
                gameNumber -= userTry;
                Console.WriteLine($"Current number = {gameNumber}");
            }
        }
        
        Console.WriteLine($"{activeUser} win!");
        Console.Write("\n For restart enter 1\n");
        while (int.Parse(Console.ReadLine()) == 1) 
        {
            gameNumber = random.Next(12, 120);
            Console.WriteLine($"Enter new number\n");
        }
        
        Console.ReadKey();
    }
}




